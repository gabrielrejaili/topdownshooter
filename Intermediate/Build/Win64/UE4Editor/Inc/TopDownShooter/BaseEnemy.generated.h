// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_BaseEnemy_generated_h
#error "BaseEnemy.generated.h already included, missing '#pragma once' in BaseEnemy.h"
#endif
#define TOPDOWNSHOOTER_BaseEnemy_generated_h

#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseEnemy(); \
	friend struct Z_Construct_UClass_ABaseEnemy_Statics; \
public: \
	DECLARE_CLASS(ABaseEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseEnemy)


#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABaseEnemy(); \
	friend struct Z_Construct_UClass_ABaseEnemy_Statics; \
public: \
	DECLARE_CLASS(ABaseEnemy, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseEnemy)


#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseEnemy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseEnemy(ABaseEnemy&&); \
	NO_API ABaseEnemy(const ABaseEnemy&); \
public:


#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseEnemy(ABaseEnemy&&); \
	NO_API ABaseEnemy(const ABaseEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseEnemy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseEnemy)


#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_11_PROLOG
#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_INCLASS \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BaseEnemy_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class ABaseEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_BaseEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
