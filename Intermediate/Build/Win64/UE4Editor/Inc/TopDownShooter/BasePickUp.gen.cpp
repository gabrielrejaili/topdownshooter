// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TopDownShooter/BasePickUp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBasePickUp() {}
// Cross Module References
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_ABasePickUp_NoRegister();
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_ABasePickUp();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TopDownShooter();
// End Cross Module References
	void ABasePickUp::StaticRegisterNativesABasePickUp()
	{
	}
	UClass* Z_Construct_UClass_ABasePickUp_NoRegister()
	{
		return ABasePickUp::StaticClass();
	}
	struct Z_Construct_UClass_ABasePickUp_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABasePickUp_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TopDownShooter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABasePickUp_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BasePickUp.h" },
		{ "ModuleRelativePath", "BasePickUp.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABasePickUp_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABasePickUp>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABasePickUp_Statics::ClassParams = {
		&ABasePickUp::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABasePickUp_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABasePickUp_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABasePickUp()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABasePickUp_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABasePickUp, 1190584346);
	template<> TOPDOWNSHOOTER_API UClass* StaticClass<ABasePickUp>()
	{
		return ABasePickUp::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABasePickUp(Z_Construct_UClass_ABasePickUp, &ABasePickUp::StaticClass, TEXT("/Script/TopDownShooter"), TEXT("ABasePickUp"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABasePickUp);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
