// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_BasePickUp_generated_h
#error "BasePickUp.generated.h already included, missing '#pragma once' in BasePickUp.h"
#endif
#define TOPDOWNSHOOTER_BasePickUp_generated_h

#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABasePickUp(); \
	friend struct Z_Construct_UClass_ABasePickUp_Statics; \
public: \
	DECLARE_CLASS(ABasePickUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABasePickUp)


#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABasePickUp(); \
	friend struct Z_Construct_UClass_ABasePickUp_Statics; \
public: \
	DECLARE_CLASS(ABasePickUp, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABasePickUp)


#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABasePickUp(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABasePickUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasePickUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasePickUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasePickUp(ABasePickUp&&); \
	NO_API ABasePickUp(const ABasePickUp&); \
public:


#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasePickUp(ABasePickUp&&); \
	NO_API ABasePickUp(const ABasePickUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasePickUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasePickUp); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABasePickUp)


#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_9_PROLOG
#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_INCLASS \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BasePickUp_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class ABasePickUp>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_BasePickUp_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
