// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_BaseProjectile_generated_h
#error "BaseProjectile.generated.h already included, missing '#pragma once' in BaseProjectile.h"
#endif
#define TOPDOWNSHOOTER_BaseProjectile_generated_h

#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseProjectile(); \
	friend struct Z_Construct_UClass_ABaseProjectile_Statics; \
public: \
	DECLARE_CLASS(ABaseProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseProjectile)


#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABaseProjectile(); \
	friend struct Z_Construct_UClass_ABaseProjectile_Statics; \
public: \
	DECLARE_CLASS(ABaseProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseProjectile)


#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseProjectile(ABaseProjectile&&); \
	NO_API ABaseProjectile(const ABaseProjectile&); \
public:


#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseProjectile(ABaseProjectile&&); \
	NO_API ABaseProjectile(const ABaseProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseProjectile)


#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_10_PROLOG
#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_INCLASS \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_BaseProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class ABaseProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_BaseProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
