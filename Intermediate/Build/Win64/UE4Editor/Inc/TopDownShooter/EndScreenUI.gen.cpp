// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TopDownShooter/UIScripts/EndScreenUI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEndScreenUI() {}
// Cross Module References
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_UEndScreenUI_NoRegister();
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_UEndScreenUI();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_TopDownShooter();
// End Cross Module References
	void UEndScreenUI::StaticRegisterNativesUEndScreenUI()
	{
	}
	UClass* Z_Construct_UClass_UEndScreenUI_NoRegister()
	{
		return UEndScreenUI::StaticClass();
	}
	struct Z_Construct_UClass_UEndScreenUI_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEndScreenUI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_TopDownShooter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEndScreenUI_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "UIScripts/EndScreenUI.h" },
		{ "ModuleRelativePath", "UIScripts/EndScreenUI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEndScreenUI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEndScreenUI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEndScreenUI_Statics::ClassParams = {
		&UEndScreenUI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEndScreenUI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEndScreenUI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEndScreenUI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEndScreenUI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEndScreenUI, 3317080696);
	template<> TOPDOWNSHOOTER_API UClass* StaticClass<UEndScreenUI>()
	{
		return UEndScreenUI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEndScreenUI(Z_Construct_UClass_UEndScreenUI, &UEndScreenUI::StaticClass, TEXT("/Script/TopDownShooter"), TEXT("UEndScreenUI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEndScreenUI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
