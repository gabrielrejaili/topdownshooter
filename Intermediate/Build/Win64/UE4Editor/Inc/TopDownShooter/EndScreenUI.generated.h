// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_EndScreenUI_generated_h
#error "EndScreenUI.generated.h already included, missing '#pragma once' in EndScreenUI.h"
#endif
#define TOPDOWNSHOOTER_EndScreenUI_generated_h

#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEndScreenUI(); \
	friend struct Z_Construct_UClass_UEndScreenUI_Statics; \
public: \
	DECLARE_CLASS(UEndScreenUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(UEndScreenUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUEndScreenUI(); \
	friend struct Z_Construct_UClass_UEndScreenUI_Statics; \
public: \
	DECLARE_CLASS(UEndScreenUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(UEndScreenUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEndScreenUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEndScreenUI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEndScreenUI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEndScreenUI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEndScreenUI(UEndScreenUI&&); \
	NO_API UEndScreenUI(const UEndScreenUI&); \
public:


#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEndScreenUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEndScreenUI(UEndScreenUI&&); \
	NO_API UEndScreenUI(const UEndScreenUI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEndScreenUI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEndScreenUI); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEndScreenUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_12_PROLOG
#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_INCLASS \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class UEndScreenUI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_UIScripts_EndScreenUI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
