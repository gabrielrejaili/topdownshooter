// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_ShooterPlayer_generated_h
#error "ShooterPlayer.generated.h already included, missing '#pragma once' in ShooterPlayer.h"
#endif
#define TOPDOWNSHOOTER_ShooterPlayer_generated_h

#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_EVENT_PARMS
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_CALLBACK_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterPlayer(); \
	friend struct Z_Construct_UClass_AShooterPlayer_Statics; \
public: \
	DECLARE_CLASS(AShooterPlayer, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(AShooterPlayer)


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAShooterPlayer(); \
	friend struct Z_Construct_UClass_AShooterPlayer_Statics; \
public: \
	DECLARE_CLASS(AShooterPlayer, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(AShooterPlayer)


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterPlayer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterPlayer(AShooterPlayer&&); \
	NO_API AShooterPlayer(const AShooterPlayer&); \
public:


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterPlayer(AShooterPlayer&&); \
	NO_API AShooterPlayer(const AShooterPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterPlayer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterPlayer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShooterPlayer)


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_11_PROLOG \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_EVENT_PARMS


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_CALLBACK_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_INCLASS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_CALLBACK_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_ShooterPlayer_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class AShooterPlayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_ShooterPlayer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
