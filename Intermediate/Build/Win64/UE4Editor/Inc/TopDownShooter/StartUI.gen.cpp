// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TopDownShooter/UIScripts/StartUI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStartUI() {}
// Cross Module References
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_UStartUI_NoRegister();
	TOPDOWNSHOOTER_API UClass* Z_Construct_UClass_UStartUI();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_TopDownShooter();
// End Cross Module References
	static FName NAME_UStartUI_FadeOut = FName(TEXT("FadeOut"));
	void UStartUI::FadeOut()
	{
		ProcessEvent(FindFunctionChecked(NAME_UStartUI_FadeOut),NULL);
	}
	void UStartUI::StaticRegisterNativesUStartUI()
	{
	}
	struct Z_Construct_UFunction_UStartUI_FadeOut_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UStartUI_FadeOut_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "UIScripts/StartUI.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UStartUI_FadeOut_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UStartUI, nullptr, "FadeOut", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UStartUI_FadeOut_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UStartUI_FadeOut_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UStartUI_FadeOut()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UStartUI_FadeOut_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UStartUI_NoRegister()
	{
		return UStartUI::StaticClass();
	}
	struct Z_Construct_UClass_UStartUI_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UStartUI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_TopDownShooter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UStartUI_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UStartUI_FadeOut, "FadeOut" }, // 2124822184
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UStartUI_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "UIScripts/StartUI.h" },
		{ "ModuleRelativePath", "UIScripts/StartUI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UStartUI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UStartUI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UStartUI_Statics::ClassParams = {
		&UStartUI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UStartUI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UStartUI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UStartUI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UStartUI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UStartUI, 639458231);
	template<> TOPDOWNSHOOTER_API UClass* StaticClass<UStartUI>()
	{
		return UStartUI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UStartUI(Z_Construct_UClass_UStartUI, &UStartUI::StaticClass, TEXT("/Script/TopDownShooter"), TEXT("UStartUI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UStartUI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
