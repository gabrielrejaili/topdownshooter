// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOPDOWNSHOOTER_StartUI_generated_h
#error "StartUI.generated.h already included, missing '#pragma once' in StartUI.h"
#endif
#define TOPDOWNSHOOTER_StartUI_generated_h

#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_SPARSE_DATA
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_RPC_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_EVENT_PARMS
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_CALLBACK_WRAPPERS
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStartUI(); \
	friend struct Z_Construct_UClass_UStartUI_Statics; \
public: \
	DECLARE_CLASS(UStartUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(UStartUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUStartUI(); \
	friend struct Z_Construct_UClass_UStartUI_Statics; \
public: \
	DECLARE_CLASS(UStartUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TopDownShooter"), NO_API) \
	DECLARE_SERIALIZER(UStartUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStartUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStartUI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStartUI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStartUI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStartUI(UStartUI&&); \
	NO_API UStartUI(const UStartUI&); \
public:


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStartUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStartUI(UStartUI&&); \
	NO_API UStartUI(const UStartUI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStartUI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStartUI); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStartUI)


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_PRIVATE_PROPERTY_OFFSET
#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_12_PROLOG \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_EVENT_PARMS


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_RPC_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_CALLBACK_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_INCLASS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_PRIVATE_PROPERTY_OFFSET \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_SPARSE_DATA \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_CALLBACK_WRAPPERS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_INCLASS_NO_PURE_DECLS \
	TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOPDOWNSHOOTER_API UClass* StaticClass<class UStartUI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TopDownShooter_Source_TopDownShooter_UIScripts_StartUI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
