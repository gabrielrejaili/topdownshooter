// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseProjectile.h"
#include "Engine.h"
#include "TimerManager.h"
#include "ShooterPlayer.h"

// Sets default values
ABaseProjectile::ABaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProjectileMesh->CastShadow = false;
	SphereVisual = CreateDefaultSubobject<USphereComponent>(TEXT("SphereVisual"));
	SphereVisual->AttachTo(ProjectileMesh);
	SphereVisual->SetHiddenInGame(false);
	SphereVisual->ShapeColor = FColor (0,255,0);
	SphereVisual->SetSphereRadius(158);
}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalOffset(FVector(10,0,0));
	
}

void ABaseProjectile::SetParams(AActor* playerRef, FVector projectileSize, UStaticMesh* meshref,
	FVector projectileColor)
{
	SetActorScale3D(projectileSize);
	ProjectileMesh->SetStaticMesh(meshref);
	ProjectileMesh->SetVectorParameterValueOnMaterials("Color",projectileColor);

	UWorld* world = GetWorld();

	if (world)
	{
		world->GetTimerManager().SetTimer(TimerHandle_WaitToDestroy,this,&ABaseProjectile::TimerDestroyEvent,DestroyTime);
	}
	
}


void ABaseProjectile::TimerDestroyEvent()
{
	Destroy();
}

