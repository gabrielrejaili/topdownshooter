// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "BaseProjectile.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ABaseProjectile : public AActor
{
	GENERATED_BODY()

public:
	ABaseProjectile();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Category=ClassSetup, EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* ProjectileMesh;
	
	UPROPERTY(Category=ClassSetup, EditAnywhere, BlueprintReadWrite)
		USphereComponent* SphereVisual;
	
	UPROPERTY(Category=ClassSetup, EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AShooterPlayer> PlayerRef;

	UPROPERTY(Category=ClassSetup, EditAnywhere, BlueprintReadWrite)
		float DestroyTime;


	UPROPERTY(Category=ClassSetup, EditAnywhere, BlueprintReadWrite)
		float ProjectileSpeed;


	
	void SetParams(AActor* playerRef, FVector projectileSize, UStaticMesh* meshref,FVector projectileColor);

	FTimerHandle TimerHandle_WaitToDestroy;

	void TimerDestroyEvent();

	
};
