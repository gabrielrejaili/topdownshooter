// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayer.h"

#include "BaseProjectile.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "UIScripts/StartUI.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
// Sets default values
AShooterPlayer::AShooterPlayer()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CellMesh"));
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->AttachTo(CellMesh);
	SpringArmComponent->TargetArmLength = 1.200f;
	SpringArmComponent->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->CameraLagSpeed = 30.f;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);

	HelperArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("HelperArrow"));
	HelperArrow->bHiddenInGame = false;
	HelperArrow->AttachTo(CellMesh);


	SphereVisual = CreateDefaultSubobject<USphereComponent>(TEXT("SphereVisual"));
	SphereVisual->AttachTo(CellMesh);
	SphereVisual->SetHiddenInGame(false);
	SphereVisual->ShapeColor = FColor(0, 255.f, 0.f);
	SphereVisual->SetSphereRadius(158.f);

	PlayerReady = false;

	CellCollorAlpha = 1.f;
}

// Called to bind functionality to input
void AShooterPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("GameStart", IE_Pressed, this, &AShooterPlayer::ClearStartMenu);
	InputComponent->BindAxis("MoveForward");
	InputComponent->BindAxis("MoveRight");
	InputComponent->BindAction("Fire", IE_Pressed, this, &AShooterPlayer::FireEventPressed);
	InputComponent->BindAction("Fire", IE_Released, this, &AShooterPlayer::FireEventReleased);
}

// Called when the game starts or when spawned
void AShooterPlayer::BeginPlay()
{
	CreateUI();
	Super::BeginPlay();
	if (StartUI != nullptr)
	{
		StartUI->AddToViewport(9999);
	}
}


// Called every frame
void AShooterPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PlayerReady)
	{
		if (!PlayerController)
		{
			PlayerController = GetWorld()->GetFirstPlayerController();
			PlayerController->bShowMouseCursor = true;
			bGotPlayerController = true;
		}
		MovementController(DeltaTime);


		ShootingLogic(DeltaTime);
		return;
	}
}

void AShooterPlayer::ShootingLogic(float deltaTime)
{
	UWorld* world = GetWorld();
	if (world)
	{
		SetScaleColor(deltaTime);

		ECollisionChannel trace = ECC_Visibility;
		bool Complex = false;
		FHitResult Hit;
		PlayerController->GetHitResultUnderCursor(trace, Complex, Hit);
		MouseHitPos = Hit.ImpactPoint;
		HelperArrow->SetWorldRotation(
			FRotator(0, (UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), MouseHitPos).Yaw), 0));
		
		if (IsFirePressed)
		{
			if (CanFire)
			{
				ABaseProjectile* Projectile = world->SpawnActor<ABaseProjectile>(
					HelperArrow->GetComponentLocation() + HelperArrow->GetForwardVector() * 10.f,
					HelperArrow->GetComponentRotation());
				Projectile->SetParams(this, GetActorScale3D() / 2, CellMesh->GetStaticMesh(),
				                      CurrentCellColor);
				TargetCellSize = TargetCellSize - 0.003f;
				CellCollorAlpha = CellCollorAlpha - 0.003f;
				CellCollorAlpha = FMath::Clamp(CellCollorAlpha, 0.f, 1.f);
				CanFire = false;
				world->GetTimerManager().SetTimer(TimerHandle_FireTimerExpired, this, &AShooterPlayer::FireTimerExpired,FireCD);
			}
		}

	}
}


void AShooterPlayer::MovementController(float DeltaTime)
{
	if (TargetCellSize > 0.0001f)
	{
		HandleWorldOffset(CurrentSpeed);
		if (InputComponent->GetAxisValue("MoveForward") > 0)
		{
			CurrentSpeed.X = FMath::FInterpTo(CurrentSpeed.X, maxSpeed.X, DeltaTime, Acceleration);
		}
		else if (InputComponent->GetAxisValue("MoveForward") < 0)
		{
			CurrentSpeed.X = FMath::FInterpTo(CurrentSpeed.X, -maxSpeed.X, DeltaTime, Acceleration);
		}
		else
		{
			CurrentSpeed.X = FMath::FInterpTo(CurrentSpeed.X, 0, DeltaTime, Acceleration);
		}


		if (InputComponent->GetAxisValue("MoveRight") > 0)
		{
			CurrentSpeed.Y = FMath::FInterpTo(CurrentSpeed.Y, maxSpeed.Y, DeltaTime, Acceleration);
		}
		else if (InputComponent->GetAxisValue("MoveRight") < 0)
		{
			CurrentSpeed.Y = FMath::FInterpTo(CurrentSpeed.Y, -maxSpeed.Y, DeltaTime, Acceleration);
		}
		else
		{
			CurrentSpeed.Y = FMath::FInterpTo(CurrentSpeed.Y, 0, DeltaTime, Acceleration);
		}
	}
}


void AShooterPlayer::ClearStartMenu()
{
	if (StartUI != nullptr)
	{
		PlayerReady = true;
		StartUI->RemoveFromParent();
	}
}

void AShooterPlayer::HandleWorldOffset(FVector velocity)
{
	CellMesh->AddWorldOffset(velocity);
}

void AShooterPlayer::FireEventPressed()
{
	IsFirePressed = true;
	CanFire = true;
}

void AShooterPlayer::FireEventReleased()
{
	IsFirePressed = false;
}

void AShooterPlayer::FireTimerExpired()
{
	CanFire = true;
}

void AShooterPlayer::SetScaleColor(float deltaTime)
{
	SetActorScale3D(FVector(
		FMath::FInterpTo(GetActorScale3D().X, TargetCellSize, deltaTime, 2.f),
		FMath::FInterpTo(GetActorScale3D().Y, TargetCellSize, deltaTime, 2.f),
		FMath::FInterpTo(GetActorScale3D().Z, TargetCellSize, deltaTime, 2.f)));

		CurrentCellColor = FMath::Lerp(FVector(1, 0, 0), FVector(1, 1, 1), CellCollorAlpha);
		CellMesh->SetVectorParameterValueOnMaterials("Color", CurrentCellColor);
}
