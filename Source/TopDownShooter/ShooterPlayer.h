// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "ShooterPlayer.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API AShooterPlayer : public APawn
{
	GENERATED_BODY()

public:
	AShooterPlayer();

protected:
	virtual void BeginPlay() override;
	void ShootingLogic(float deltaTime);
	void MovementController(float DeltaTime);

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UPROPERTY(Category= UserInterface,EditAnywhere,BlueprintReadWrite)
	class UStartUI *StartUI;
	 
	UFUNCTION(BlueprintImplementableEvent)
		void CreateUI();
	
	void ClearStartMenu();

	void HandleWorldOffset(FVector velocity);


	
	void FireEventPressed();
	void FireEventReleased();
	void FireTimerExpired();
	FTimerHandle TimerHandle_FireTimerExpired;





	
	UPROPERTY(Category= ClassSetup,EditAnywhere,BlueprintReadWrite)
		UStaticMeshComponent *CellMesh;
	
	UPROPERTY(Category= ClassSetup,EditAnywhere,BlueprintReadWrite)
		class USpringArmComponent *SpringArmComponent;

	UPROPERTY(Category= ClassSetup,EditAnywhere,BlueprintReadWrite)
		class UCameraComponent* CameraComponent;

	UPROPERTY(Category= ClassSetup,EditAnywhere,BlueprintReadWrite)
		USphereComponent *SphereVisual;

	
	UArrowComponent* HelperArrow;
	bool PlayerReady;
	bool bGotPlayerController;
	APlayerController *PlayerController;

	UPROPERTY(Category= Speed,EditAnywhere,BlueprintReadWrite)
	FVector maxSpeed;
	FVector CurrentSpeed;
	UPROPERTY(Category= Speed,EditAnywhere,BlueprintReadWrite)
	float Acceleration;
	
	UPROPERTY(Category= ClassSettings,EditAnywhere,BlueprintReadWrite)
	float TargetCellSize;

	UPROPERTY(Category= ProjectileSettings,EditAnywhere,BlueprintReadWrite)

	FVector CurrentCellColor;
	

	void SetScaleColor(float deltaTime);
	float CellCollorAlpha;
	

	UPROPERTY(Category= ProjectileSettings,EditAnywhere,BlueprintReadWrite)
    float FireCD;
    		
	bool IsFirePressed;
	bool CanFire;
	FVector MouseHitPos;
	
};
