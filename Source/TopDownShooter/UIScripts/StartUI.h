// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "StartUI.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNSHOOTER_API UStartUI : public UUserWidget
{
	GENERATED_BODY()

	/**
 * @brief 
 */
public:
UFUNCTION(BlueprintImplementableEvent)
	void FadeOut();
	
};
